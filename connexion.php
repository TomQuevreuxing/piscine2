<?php

	
	session_start ();
	
	//On récupère les valeurs du formulaires
	$identifiant = isset($_POST["Pseudo"])?$_POST["Pseudo"]:"";
    $email = isset($_POST["Email"])?$_POST["Email"]:"";
	$connexion = false;//Variable pour vérifier si la connexion est bonne

	
	if(filter_var($email, FILTER_VALIDATE_EMAIL))//On vérifie s'il s'agit bien d'une adresse mail
	{
            $connexion = true;
			
    }
	
	if($connexion == false)
	{
		echo 'Addresse mail invalide';
	}
	else
	{
			if( $identifiant != "" && $email != "")//Si les champs ne sont pas vides
			{
				try//On ouvre la base
				{
					$bdd = new PDO('mysql:host=localhost;dbname=piscine', 'root', '');
				}
				catch(Exception $e)//Si erreur
				{
					die('Erreur log : ' . $e->getMessage());
				}
				  
				$req = $bdd->query(' SELECT * FROM profil' );//Requete SQL
				  
				$trouve;//Variable pour vérifier si c'est bien dans la base
				  
				while($donnees = $req->fetch())
				{
					$pseudo = $donnees['pseudo'];
					$mail = $donnees['mail'];
					$id_pseudo = $donnees['profil_id'];
					$admin = $donnees['admin'];
					
					if($pseudo == $identifiant && $mail == $email)//si les identifiant son dans la bdd
					{
						$trouve = true;
						break;
					}
					else
					{
						$trouve = false;
					}
					
				}
				
				if($trouve == true && $admin == false)
				{
					$_SESSION['id_pseudo'] = $id_pseudo;  //connexion utilisateur
					header('Location: acceuil.php'); // Envoi vers la page d'accueil
					echo 'Connexion utilisateur réussie';
				}
				
				if($trouve == true && $admin == true)
				{
					$_SESSION['id_pseudo'] = $id_pseudo; //connexion administrateur
					header('Location: admin.php'); //Envoi vers la page admin
					echo 'Connexion admin réussie';
				}
				
				if($trouve == false)
				{
					echo 'Pseudo ou Email incorrect';
				}
				$req->closeCursor();
			}
		else
		{
			echo'Un champ ou plus est vide';
		}
	}


?>